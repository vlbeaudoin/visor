package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"libvirt.org/go/libvirt"

	"codeberg.org/vlbeaudoin/couleuvre"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var connection *libvirt.Connect

var app couleuvre.App[Config]

type Config struct {
	LibvirtConnectURI string
	ServerAPIKey      string
	ServerCertFile    string
	ServerKeyFile     string
	ServerHost        string
	ServerPort        uint
}

func init() {
	app = couleuvre.NewApp[Config]("VISOR_", ".", "_")
	flag.StringVar(&app.Config.LibvirtConnectURI, "libvirtconnecturi", "qemu:///system", "libvirt connect URI")
	flag.StringVar(&app.Config.ServerAPIKey, "serverapikey", "visor", "API server key")
	flag.StringVar(&app.Config.ServerHost, "serverhost", "localhost", "Server hostname or ip")
	flag.UintVar(&app.Config.ServerPort, "serverport", 1312, "API and web server port")
	flag.StringVar(&app.Config.ServerCertFile, "servercertfile", "cert.pem", "TLS certificate file")
	flag.StringVar(&app.Config.ServerKeyFile, "serverkeyfile", "key.pem", "TLS private key file")
}

const (
	HealthCmdName = "health"
	HealthCmdDesc = "View server health"

	PaveCmdName = "pave"
	PaveCmdDesc = "View pave spec"

	DomListCmdName = "dom-list"
	DomListCmdDesc = "View domains"

	PoolListCmdName = "pool-list"
	PoolListCmdDesc = "View pools"

	NetListCmdName = "net-list"
	NetListCmdDesc = "View networks"

	ServerCmdName = "server"
	ServerCmdDesc = "Start visor server daemon"
)

func main() {
	// Parse config, envs, and flags
	if err := app.Parse(); err != nil {
		log.Fatal(err)
	}

	// Commands
	if err := app.NewCommand(HealthCmdName, HealthCmdDesc, HealthCmdExecuter); err != nil {
		log.Fatal(err)
	}

	if err := app.NewCommand(ServerCmdName, ServerCmdDesc, ServerCmdExecuter); err != nil {
		log.Fatal(err)
	}

	// Parse command
	var commandName string
	if len(os.Args) > 1 {
		commandName = flag.Args()[0]
	}

	found, err := app.ParseCommand(commandName)
	if err != nil {
		log.Fatal(err)
	}

	connection, err = libvirt.NewConnect(app.Config.LibvirtConnectURI)
	if err != nil {
		log.Fatal(err)
	}

	// Execute command
	if err := found.Execute(); err != nil {
		log.Fatal(err)
	}
}

// Returns libvirt node information
func GetHealth() (map[string]any, error) {
	nodeInfo, err := connection.GetNodeInfo()
	if err != nil {
		return nil, err
	}

	domains, err := GetDomains()
	if err != nil {
		return nil, err
	}

	var domainsActive uint

	for _, domain := range domains {
		isActive, err := domain.IsActive()
		if err != nil {
			return nil, err
		}
		if isActive {
			domainsActive++
		}
	}

	hostname, err := connection.GetHostname()
	if err != nil {
		return nil, err
	}

	var poolsActive uint

	pools, err := GetPools()
	if err != nil {
		return nil, err
	}

	for _, pool := range pools {
		isActive, err := pool.IsActive()
		if err != nil {
			return nil, err
		}

		if isActive {
			poolsActive++
		}
	}

	var netsActive uint

	nets, err := GetNets()
	if err != nil {
		return nil, err
	}

	for _, net := range nets {
		isActive, err := net.IsActive()
		if err != nil {
			return nil, err
		}
		if isActive {
			netsActive++
		}
	}

	return map[string]any{
		hostname: map[string]any{
			"nodeInfo":      nodeInfo,
			"domainsActive": domainsActive,
			"domainsTotal":  len(domains),
			"poolsActive":   poolsActive,
			"poolsTotal":    len(pools),
			"netsActive":    netsActive,
			"netsTotal":     len(nets),
		},
	}, nil
}

// Returns libvirt domains
func GetDomains() ([]libvirt.Domain, error) {
	return connection.ListAllDomains(libvirt.CONNECT_LIST_DOMAINS_PERSISTENT)
}

// Returns libvirt pools
func GetPools() ([]libvirt.StoragePool, error) {
	return connection.ListAllStoragePools(libvirt.CONNECT_LIST_STORAGE_POOLS_PERSISTENT)
}

// Returns libvirt networks
func GetNets() ([]libvirt.Network, error) {
	return connection.ListAllNetworks(libvirt.CONNECT_LIST_NETWORKS_PERSISTENT)
}

func HealthCmdExecuter() (err error) {
	message, err := GetHealth()
	if err != nil {
		return
	}

	fmt.Println(message)

	return
}

func ServerCmdExecuter() error {
	e := echo.New()

	e.HideBanner = true
	e.HidePort = true

	e.Pre(middleware.AddTrailingSlash())

	fmt.Printf(`==
visor is a virtualization platform based on libvirt

This project is free software licensed under GPLv2

Repo:
	https://codeberg.org/vlbeaudoin/visor
Issues:
	https://codeberg.org/vlbeaudoin/visor/issues
Wiki:
	https://codeberg.org/vlbeaudoin/visor/wiki
==
Web console:
		https://%s:%d/
API server:
		https://%s:%d/api/
==
`,
		app.Config.ServerHost, app.Config.ServerPort,
		app.Config.ServerHost, app.Config.ServerPort,
	)

	e.GET("/", func(c echo.Context) error {
		html := `<!DOCTYPE html>
<html>
<head>
    <meta charset="utf8">
    <title>visor web platform</title>
    <script src="https://unpkg.com/htmx.org@1.9.9"></script>
</head>
<body>
    <h2>Hello visor!</h2>
    <h3>healthcheck</h3>
    <code hx-trigger="load" hx-get="/api/health/"></code>
    <h3>domains</h3>
    <code hx-trigger="load" hx-get="/api/domains/"></code>
    <h3>pools</h3>
    <code hx-trigger="load" hx-get="/api/pools/"></code>
    <h3>nets</h3>
    <code hx-trigger="load" hx-get="/api/nets/"></code>
</body>
</html>
`

		return c.HTML(http.StatusOK, html)
	})

	e.GET("/api/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, map[string]string{"message": "Hello visor!"})
	})

	e.GET("/api/health/", func(c echo.Context) error {
		health, err := GetHealth()
		if err != nil {
			return c.JSON(http.StatusInternalServerError, err.Error())
		}

		return c.JSON(http.StatusOK, health)
	})

	e.GET("/api/domains/", func(c echo.Context) error {
		domains, err := GetDomains()
		if err != nil {
			return c.JSON(http.StatusInternalServerError, err.Error())
		}

		return c.JSON(http.StatusOK, domains)
	})

	e.GET("/api/pools/", func(c echo.Context) error {
		pools, err := GetPools()
		if err != nil {
			return c.JSON(http.StatusInternalServerError, err.Error())
		}

		result := make(map[string]any)

		for _, pool := range pools {
			uuid, err := pool.GetUUIDString()
			if err != nil {
				return c.JSON(http.StatusInternalServerError, err.Error())
			}

			name, err := pool.GetName()
			if err != nil {
				return c.JSON(http.StatusInternalServerError, err.Error())
			}

			isActive, err := pool.IsActive()
			if err != nil {
				return c.JSON(http.StatusInternalServerError, err.Error())
			}

			autostart, err := pool.GetAutostart()
			if err != nil {
				return c.JSON(http.StatusInternalServerError, err.Error())
			}

			result[name] = map[string]any{
				"uuid":      uuid,
				"isActive":  isActive,
				"autostart": autostart,
			}
		}

		return c.JSON(http.StatusOK, result)
	})

	e.GET("/api/nets/", func(c echo.Context) error {
		nets, err := GetNets()
		if err != nil {
			return c.JSON(http.StatusInternalServerError, err.Error())
		}

		result := make(map[string]any)

		for _, net := range nets {
			uuid, err := net.GetUUIDString()
			if err != nil {
				return c.JSON(http.StatusInternalServerError, err.Error())
			}

			name, err := net.GetName()
			if err != nil {
				return c.JSON(http.StatusInternalServerError, err.Error())
			}

			isActive, err := net.IsActive()
			if err != nil {
				return c.JSON(http.StatusInternalServerError, err.Error())
			}

			autostart, err := net.GetAutostart()
			if err != nil {
				return c.JSON(http.StatusInternalServerError, err.Error())
			}

			result[name] = map[string]any{
				"uuid":      uuid,
				"isActive":  isActive,
				"autostart": autostart,
			}
		}

		return c.JSON(http.StatusOK, result)
	})

	log.Fatal(e.StartTLS(fmt.Sprintf(":%d", app.Config.ServerPort), app.Config.ServerCertFile, app.Config.ServerKeyFile))

	return nil
}
