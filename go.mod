module codeberg.org/vlbeaudoin/visor

go 1.21.3

require (
	codeberg.org/vlbeaudoin/couleuvre v0.10.0
	github.com/labstack/echo/v4 v4.11.3
	libvirt.org/go/libvirt v1.9008.0
)

require (
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/labstack/gommon v0.4.1 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/rogpeppe/go-internal v1.11.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	golang.org/x/crypto v0.16.0 // indirect
	golang.org/x/net v0.19.0 // indirect
	golang.org/x/sys v0.15.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/time v0.5.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
