# visor

Experimental virtualization platform based on libvirt

## prerequisites

### debian 12

```
$ apt install libvirt-daemon-system libvirt-dev qemu-system qemu-utils
```

## quickstart

Visor will only run behind TLS, and so requires a TLS certificate and private key.

It makes no distinction between a verified or a self-signed pair, and so a quick way to get started is with the following command (provided you have `openssl` installed)

```
$ openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes
```

*Don't forget to make the files world un-readeable, ESPECIALLY if the files are signed*

Then, either bring in an executable from a build server or build it from source (requires `go v1.21+`)

```
$ go install .
```

*Default install location: `$HOME/go/bin/visor`*

The resulting executable can be explored by calling it without command:

```
$ visor
```

To start the server, use the `server` command

```
$ visor server
```

The project also comes with a [systemd service file](visord.service) that requires `visor` to be installed at `/usr/bin/visor`.

Install it at `/etc/systemd/system/` to manage visor with systemd.
